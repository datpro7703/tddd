import tensorflow as tf
import zipfile
from urllib.request import urlopen
import cv2
import numpy as np
import os
from random import shuffle
from tqdm import tqdm, notebook
import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression
import matplotlib.pyplot as plt
from tensorflow.python.framework import ops
import requests
import re

ops.reset_default_graph()

zipresp = urlopen("https://tddd.s3.ap-southeast-1.amazonaws.com/dataset.zip")
tempzip = open("dataset.zip", "wb")
tempzip.write(zipresp.read())
tempzip.close()

zip_ref = zipfile.ZipFile("dataset.zip", 'r')
zip_ref.extractall("/dataset")
zip_ref.close()

TRAIN_DIR = '/dataset/Eye Dataset/Train_Data'
TEST_DIR = '/dataset/Eye Dataset/Test_Data'
TEST_DIR2 = '/dataset/Eye Dataset/Validate_Data'
IMG_SIZE = 50
LR = 1e-3
MODEL_NAME = 'dnn_models/EyeDet.model'.format(LR, '2conv-basic')

def label_img(img):
    word_label = img.split('_')[4]
    if word_label[0] == '1': return [1,0] 
    elif word_label[0] == '0': return [0,1]
    
    #[1,0] eyes are open
    #[0,1] eyes are closed

def create_train_data():
    training_data = []
    for img in tqdm(os.listdir(TRAIN_DIR)):
        label = label_img(img)
        path = os.path.join(TRAIN_DIR, img)
        img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
        training_data.append([np.array(img), np.array(label)])
    shuffle(training_data)
    np.save('train_data.npy', training_data)
    return training_data

def create_test_data():
    testing_data = []
    for img in tqdm(os.listdir(TEST_DIR)):
        label = label_img(img)
        path = os.path.join(TEST_DIR, img)
        img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
        testing_data.append([np.array(img), np.array(label)])
    shuffle(testing_data)
    np.save('test_data.npy', testing_data)
    return testing_data

def create_test_data2():
    testing_data = []
    for img in tqdm(os.listdir(TEST_DIR2)):
        label = label_img(img)
        path = os.path.join(TEST_DIR2, img)
        img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
        img = cv2.resize(img, (IMG_SIZE, IMG_SIZE))
        testing_data.append([np.array(img), np.array(label)])
    shuffle(testing_data)
    np.save('test_data2.npy', testing_data)
    return testing_data

train_data = create_train_data()
test_data = create_test_data()

convnet = input_data(shape=[None, IMG_SIZE, IMG_SIZE, 1], name='input')

convnet = conv_2d(convnet, 32, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)

convnet = conv_2d(convnet, 64, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)

convnet = conv_2d(convnet, 128, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)

convnet = conv_2d(convnet, 64, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)

convnet = conv_2d(convnet, 32, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)

convnet = fully_connected(convnet, 1024, activation='relu')
convnet = dropout(convnet, 0.8)

convnet = fully_connected(convnet, 2, activation='softmax')
convnet = regression(convnet, optimizer='adam', learning_rate=LR,
                     loss='categorical_crossentropy', name='targets')

model = tflearn.DNN(convnet, tensorboard_dir='log')

train = train_data
test = test_data

X = np.array([i[0] for i in train]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
Y = [i[1] for i in train]

test_x = np.array([i[0] for i in test]).reshape(-1, IMG_SIZE, IMG_SIZE, 1)
test_y = [i[1] for i in test]

model.fit({'input': X}, {'targets': Y}, n_epoch=5,
          validation_set=({'input': test_x}, {'targets': test_y}),
          snapshot_step=500, show_metric=True, run_id=MODEL_NAME)
model.save(MODEL_NAME)

test_data = create_test_data2()
# if you already have some saved:
#test_data = np.load('test_data.npy')

fig=plt.figure()

for num,data in enumerate(test_data[:12]):
    # open: [1,0]
    # closed: [0,1]
    
    img_num = data[1]
    img_data = data[0]
    
    y = fig.add_subplot(3,4,num+1)
    orig = img_data
    data = img_data.reshape(IMG_SIZE,IMG_SIZE,1)
    model_out = model.predict([data])[0]
    
    if np.argmax(model_out) == 1: str_label='Closed'
    else: str_label='Open'
        
    y.imshow(orig,cmap='gray')
    plt.title(str_label)
    y.axes.get_xaxis().set_visible(False)
    y.axes.get_yaxis().set_visible(False)

def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file), 
                       os.path.relpath(os.path.join(root, file), 
                                       os.path.join(path, '..')))
      
zipf = zipfile.ZipFile('fw.zip', 'w', zipfile.ZIP_DEFLATED)
zipdir('dnn_model/', zipf)
zipf.close()

def file_extractor(file_path):
    with open(file_path, 'r') as f:
        matches = re.findall(r'^(.+)=(.*)$', f.read(), flags=re.M)
        d = dict(matches)
    f.close
    return d

def upload_firmware():
    api_url = "https://dhdev-drowsiness123.herokuapp.com/api/v1/firmwares"

    d = file_extractor("tmp.txt")
    timeDetection = d["timeDetection"]
    description = d["description"]

    files = {'file': open("fw.zip",'rb')}
    headers = {
        'Accept':'application/json'
    }
    json_data = {
        'timeDetection':timeDetection,
        'description':description,
    }
    
    response = requests.post(api_url, data=json_data , files=files, headers=headers)
    return response


response = upload_firmware()
print(response.content)
#plt.show()
exit()






